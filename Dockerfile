FROM debian:testing-slim

RUN apt update && apt install -y bluetooth bluez bluez-firmware libbluetooth-dev git supervisor build-essential nginx
RUN git clone https://github.com/wcbonner/GoveeBTTempLogger.git && cd GoveeBTTempLogger && make deb && dpkg -i ./GoveeBTTempLogger.deb
RUN apt remove -y build-essential
RUN mkdir -p /etc/supervisor/conf.d
COPY supervisor.conf /etc/supervisor/conf.d/supervisord.conf

EXPOSE 3000

CMD ["/usr/bin/supervisord"]

